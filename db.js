var Sequelize = require('sequelize');

var db = new Sequelize('restfulChat', 'sergey', 'password',{
    dialect: 'sqlite'
});

module.exports = db;

var User = require('./models/User');
var Post = require('./models/Post');

Post.belongsTo(User, {foreignKey: 'user_id'});
