var express     = require('express'),
    path        = require('path'),
    bodyParser  = require('body-parser'),
    session     = require('express-session'),
    Sequelize   = require('sequelize'),
    db          = require('./db'),
    Post        = require('./models/Post'),
    User        = require('./models/User');


// Init app and start the server on the same port: 3000
var app = express();
var server = app.listen(3000, function(){
    console.log('The Chat server started at: http://127.0.0.1:3000');
    db.sync({logging: console.log});
});

// View Engine
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// Static Folder
app.use(express.static(path.join(__dirname, 'public')));

// BodyParser
app.use(bodyParser.urlencoded({extended: true}));

// Express Session
app.use(session({
    secret: 'secret',
    saveUninitialized: true,
    resave: true,
}));

// SocketIO config
var io = require('socket.io').listen(server);
var usernames = [];
var connections = [];

io.on('connection', function(socket){
    connections.push(socket);
    console.log('Connected: %s sockets connected', connections.length);

    // Disconnect
    socket.on('disconnect', function(data){
        usernames.splice(usernames.indexOf(socket.username), 1);
        updateUsers();
        connections.splice(connections.indexOf(socket), 1);
        console.log('Disconnected: %s sockets connected', connections.length);
    });

    // Send Message
    socket.on('send message', function(data){
        Post.create({
            text: data.message,
            user_id: socket.userID
        }).then(function(post){
            io.sockets.emit('new message', {message: post.text, username: socket.username});
        }).catch(function(error){
            io.sockets.emit('message validation', {error: error.errors[0].message});
        });
    });

    // New User
    socket.on('new user', function(data, callback){
        User.create({
            name: data
        }).then(function(user){
            callback(true);
            socket.username = user.name;
            socket.userID = user.id;
            usernames.push(user.name);
            updateUsers();
        }).catch(function(error){
            io.sockets.emit('user validation', {error: error.errors[0].message});
        });
    });

    function updateUsers(){
        io.sockets.emit('get users', {usernames: usernames});
    }
});

// Routes imports
var chat = require('./routes/chat');
var posts = require('./routes/posts');

// Routes Config
app.use('/', chat);
app.use('/posts', posts);
