var Sequelize = require('sequelize');
var db = require('../db');

var Post = db.define('post', {
    text: {
        type: Sequelize.TEXT,
        validate: {
            len: {
                args: [1, 200],
                msg: 'Post field can not be blank. Please type a post from 1 to 200 characters.'
            }
        },
        allowNull: false
    }
});

module.exports = Post;
