var Sequelize = require('sequelize');
var db = require('../db');

var User = db.define('user', {
    name: {
        type: Sequelize.STRING,
        validate: {
            isAlphanumeric: {
                msg: 'Username field can not be blank. Please use numbers and characters for Username.'
            }
        },
        allowNull: false
    }
});

module.exports = User;
