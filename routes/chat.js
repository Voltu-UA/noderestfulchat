var express = require('express');
var router = express.Router();

// Chat page
router.get('/', function(req, res){
    res.render('chat');
});

module.exports = router;
