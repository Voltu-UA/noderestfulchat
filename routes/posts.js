var express = require('express');
var router = express.Router();
var Post = require('../models/Post');
var User = require('../models/User');

// All posts
router.get('/', function(req, res){
    var posts = [];
    Post.findAll({include: {all: true}}).then(function(post){
        post.forEach(function(post){
            posts.push({text: post.dataValues.text, username: post.dataValues.user.dataValues.name});
        });
        res.render('posts', {posts: posts});
    });
});

// Post form
router.get('/new', function(req, res){
    if(req.session.error){
        var error = req.session.error;
    }
    delete req.session.error;
    res.render('new', {error: error || null});
});

// Create post
router.post('/', function(req, res){
    User.create({name: req.body.username}).then(function(user){
        Post.create({
            text: req.body.post,
            user_id: user.dataValues.id
        }).then(function(post){
            res.redirect('/posts');
        }).catch(function(error){
            req.session.error = error.errors[0].message;
            res.redirect('/posts/new');
        });
    }).catch(function(error){
        req.session.error = error.errors[0].message;
        res.redirect('/posts/new');
    });
});

module.exports = router;
