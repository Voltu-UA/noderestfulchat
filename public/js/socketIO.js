if($(location).attr('href') === 'http://127.0.0.1:3000/'){
    $(function(){
        var socket = io();
        var $messageForm = $('#messageForm');
        var $message = $('#message');
        var $chat = $('#chat');
        var $userFormArea = $('#userFormArea');
        var $userForm = $('#userForm');
        var $messageArea = $('#messageArea');
        var $users = $('#users');
        var $username = $('#username');

        $messageForm.submit(function(e){
            e.preventDefault();
            socket.emit('send message', {message: $message.val()});
            $message.val('');
        });

        socket.on('new message', function(data){
            $chat.append('<div class="well"><strong>' + data.username + '</strong>:' + data.message + '<div>');
        });

        socket.on('message validation', function(data){
            $message.attr('placeholder', data.error);
        });

        socket.on('user validation', function(data){
            $username.attr('placeholder', data.error);
        });

        $userForm.submit(function(e){
            e.preventDefault();
            socket.emit('new user', $username.val(), function(data){
                if(data){
                    $userFormArea.hide();
                    $messageArea.show();
                }
            });
            $username.val('');
        });

        socket.on('get users', function(data){
            var html = '';
            for (i = 0; i < data.usernames.length; i++){
                html += '<li class="list-group-item">' + data.usernames[i] + '</li>';
            }
            $users.html(html);
        });
    });
}
