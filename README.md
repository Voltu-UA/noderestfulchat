Restful chat application.

Stack: Node.js/Express.js/SocketIO.js/jQuery/Bootstrap3/SQLite with in memory mode.

Setup:

1. clone the repository

2. cd to project directory (**nodeRestfulChat**)

3. install all necessary packages
```

npm insall --save
```

To run the server:

from the main folder of the project (**nodeRestfulChat**) run

```

node app.js
```

For the best user experience on the same local machine, use browser's "incognito" window for the second, third, e.t.c users of the chat.

Visit http://127.0.0.1:3000/ in you web browser to see the login page.

You can also use postman collection file to test REST api, but application has a user friendly UI for this purposes.

To create a new post visit: http://127.0.0.1:3000/posts/new page.

To get all posts from the chat DB visit: http://127.0.0.1:3000/posts page.